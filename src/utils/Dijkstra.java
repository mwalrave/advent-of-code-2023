package utils;

import java.util.*;

public class Dijkstra {

    static class Node {
        public int x;
        public int y;
        boolean isPlanet;
        Node north;
        Node south;
        Node west;
        Node east;
        private List<Node> shortestPath = new LinkedList<>();

        private Integer distance = Integer.MAX_VALUE;

        Map<Node, Integer> adjacentNodes = new HashMap<>();

        public Node(int x, int y, boolean isPlanet) {
            this.x = x;
            this.y = y;
            this.isPlanet = isPlanet;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public void setNeighbours(Node n, Node s, Node e, Node w) {
            north = n;
            south = s;
            west = w;
            east = e;
        }

        public void setDestinations() {
            if (north != null) adjacentNodes.put(north, 1);
            if (south != null) adjacentNodes.put(south, 1);
            if (west != null) adjacentNodes.put(west, 1);
            if (east != null) adjacentNodes.put(east, 1);
        }

        public void setDestinationsWithTarget(Node target) {
            if (north != null && target.y <= this.y) adjacentNodes.put(north, 1);
            if (south != null && target.y >= this.y) adjacentNodes.put(south, 1);
            if (west != null && target.x <= this.x) adjacentNodes.put(west, 1);
            if (east != null && target.x >= this.x) adjacentNodes.put(east, 1);
        }
    }

    public static List<Node> calculateShortestPathFromSource(List<Node> graph, Node source) {
        source.distance = 0;

        Set<Node> settledNodes = new HashSet<>();
        Set<Node> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);

        while (unsettledNodes.size() != 0) {
            Node currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Map.Entry<Node, Integer> adjacencyPair :
                    currentNode.adjacentNodes.entrySet()) {
                Node adjacentNode = adjacencyPair.getKey();
                Integer edgeWeight = adjacencyPair.getValue();
                if (!settledNodes.contains(adjacentNode)) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.add(currentNode);
        }
        return graph;
    }

    private static Node getLowestDistanceNode(Set<Node> unsettledNodes) {
        Node lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Node node : unsettledNodes) {
            int nodeDistance = node.distance;
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private static void calculateMinimumDistance(Node evaluationNode,
                                                 Integer edgeWeigh, Node sourceNode) {
        Integer sourceDistance = sourceNode.distance;
        if (sourceDistance + edgeWeigh < evaluationNode.distance) {
            evaluationNode.distance = sourceDistance + edgeWeigh;
            LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.shortestPath);
            shortestPath.add(sourceNode);
            evaluationNode.shortestPath = shortestPath;
        }
    }
}
