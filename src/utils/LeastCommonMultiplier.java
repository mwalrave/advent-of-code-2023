package utils;

public class LeastCommonMultiplier {

    /*
        The greatest common divisor (GCD) of two or more integers, which are not all zero,
        is the largest positive integer that divides each of the integers. For two integers
        x, y, the greatest common divisor of x and y is denoted gcd(x, y).
        For example, the GCD of 8 and 12 is 4, that is, gcd(8, 12) = 4.
     */
    public static long gcd(long a, long b) {
        if (b == 0)
            return a;
        return gcd(b, a % b);
    }

    // lcm() method returns the LCM of a and b
    public static long lcm(long a, long b) {
        if (a == 0 && b == 0) {
            return 0;
        }
        return a / gcd(a, b) * b;
    }
}
