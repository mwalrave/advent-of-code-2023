import days.*;
import org.junit.jupiter.api.Test;
import utils.InputConverter;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Runner {

    @Test
    void DayOne(){
        DayOne.run(INPUT_READER(DAY_ONE));
    }

    @Test
    void DayTwo(){
        DayTwo.run(INPUT_READER(DAY_TWO));
    }

    @Test
    void DayThree(){
        DayThree.run(INPUT_READER(DAY_THREE));
    }

    @Test
    void DayFour(){
        DayFour.run(INPUT_READER(DAY_FOUR));
    }

    @Test
    void DayFive(){
        DayFive.run(INPUT_READER(DAY_FIVE));
    }

    @Test
    void DaySix(){
        DaySix.run(INPUT_READER(DAY_SIX));
    }

    @Test
    void DaySeven(){
        DaySeven.run(INPUT_READER(DAY_SEVEN));
    }

    @Test
    void DayEight(){
        DayEight.run(INPUT_READER(DAY_EIGHT));
    }

    @Test
    void DayNine(){
        DayNine.run(INPUT_READER(DAY_NINE));
    }

    @Test
    void DayTen(){
        DayTen.run(INPUT_READER(DAY_TEN));
    }

    @Test
    void DayEleven(){
        DayEleven.run(INPUT_READER(DAY_ELEVEN_TEST));
    }

    @Test
    void DayTwelve(){
        DayTwelve.run(INPUT_READER(DAY_TWELVE));
    }

    @Test
    void DayThirteen(){
        DayThirteen.run(INPUT_READER(DAY_THIRTEEN));
    }

    private static List<String> INPUT_READER(String path){
        Path input = Paths.get("src/resources/" + path);
        return InputConverter.convertFileToStringList(input);
    }

    private final String DAY_THIRTEEN_TEST = "day_thirteen_input_test.txt";
    private final String DAY_THIRTEEN = "day_thirteen_input.txt";
    private final String DAY_TWELVE_TEST = "day_twelve_input_test.txt";
    private final String DAY_TWELVE = "day_twelve_input.txt";
    private final String DAY_ELEVEN_TEST = "day_eleven_input_test.txt";
    private final String DAY_ELEVEN = "day_eleven_input.txt";
    private final String DAY_TEN_TEST = "day_ten_input_test.txt";
    private final String DAY_TEN_PART_TWO_TEST = "day_ten_input_part_two_test.txt";
    private final String DAY_TEN = "day_ten_input.txt";
    private final String DAY_NINE_TEST = "day_nine_input_test.txt";
    private final String DAY_NINE = "day_nine_input.txt";
    private final String DAY_EIGHT_TEST = "day_eight_input_test.txt";
    private final String DAY_EIGHT = "day_eight_input.txt";
    private final String DAY_SEVEN_TEST = "day_seven_input_test.txt";
    private final String DAY_SEVEN = "day_seven_input.txt";
    private final String DAY_SIX_TEST = "day_six_input_test.txt";
    private final String DAY_SIX = "day_six_input.txt";
    private final String DAY_FIVE_TEST = "day_five_input_test.txt";
    private final String DAY_FIVE = "day_five_input.txt";
    private final String DAY_FOUR_TEST = "day_four_input_test.txt";
    private final String DAY_FOUR = "day_four_input.txt";
    private final String DAY_THREE_TEST = "day_three_input_test.txt";
    private final String DAY_THREE = "day_three_input.txt";
    private final String DAY_TWO_TEST = "day_two_input_test.txt";
    private final String DAY_TWO = "day_two_input.txt";
    private final String DAY_ONE_TEST = "day_one_input_test.txt";
    private final String DAY_ONE_TEST_PART_TWO = "day_one_input_part_two_test.txt";
    private final String DAY_ONE = "day_one_input.txt";
}

