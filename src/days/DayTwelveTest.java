package days;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class DayTwelveTest {
    @Test
    public void test() {
        String s = "x#######xxxxxx#xxx#x#xxxxx";
        var replaced = s.replaceAll("(x)\\1{1,}", "$1");
        assertEquals("x#######x#x#x#x", replaced);
    }

    @Test
    @Disabled
    public void roysTest() {
        Map<DayTwelve.State, Long> validationMap2 = new HashMap<>();
        assertEquals(1L, DayTwelve.recursive3(new DayTwelve.State(".##.?#??.#.?#", List.of(2, 1, 1, 1), validationMap2)));
    }

    @Test
    @Disabled
    void test3() {
        Map<DayTwelve.State, Long> validationMap2 = new HashMap<>();
        var groups1 = List.of(1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3);
        var springs1 = "???.###????.###????.###????.###????.###";
        var groups2 = List.of(1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3);
        var springs2 = ".??..??...?##.?.??..??...?##.?.??..??...?##.?.??..??...?##.?.??..??...?##";
        var groups3 = List.of(1, 3, 1, 6, 1, 3, 1, 6, 1, 3, 1, 6, 1, 3, 1, 6, 1, 3, 1, 6);
        var springs3 = "?#?#?#?#?#?#?#???#?#?#?#?#?#?#???#?#?#?#?#?#?#???#?#?#?#?#?#?#???#?#?#?#?#?#?#?";
        var groups4 = List.of(4, 1, 1, 4, 1, 1, 4, 1, 1, 4, 1, 1, 4, 1, 1);
        var springs4 = "????.#...#...?????.#...#...?????.#...#...?????.#...#...?????.#...#...";
        var groups5 = List.of(1, 6, 5, 1, 6, 5, 1, 6, 5, 1, 6, 5, 1, 6, 5);
        var springs5 = "????.######..#####.?????.######..#####.?????.######..#####.?????.######..#####.?????.######..#####.";
        var groups6 = List.of(3, 2, 1, 3, 2, 1, 3, 2, 1, 3, 2, 1, 3, 2, 1);
        var springs6 = "?###??????????###??????????###??????????###??????????###????????";

        assertEquals(1, DayTwelve.recursive3(new DayTwelve.State(springs1, groups1, validationMap2)));
        validationMap2 = new HashMap<>();
        assertEquals(16384, DayTwelve.recursive3(new DayTwelve.State(springs2, groups2, validationMap2)));
        validationMap2 = new HashMap<>();
        assertEquals(1, DayTwelve.recursive3(new DayTwelve.State(springs3, groups3, validationMap2)));
        validationMap2 = new HashMap<>();
        assertEquals(16, DayTwelve.recursive3(new DayTwelve.State(springs4, groups4, validationMap2)));
        validationMap2 = new HashMap<>();
        assertEquals(2500, DayTwelve.recursive3(new DayTwelve.State(springs5, groups5, validationMap2)));
        validationMap2 = new HashMap<>();
        assertEquals(506250, DayTwelve.recursive3(new DayTwelve.State(springs6, groups6, validationMap2)));
    }


    @Test
    void nonMultiplied() {
        Map<DayTwelve.State, Long> validationMap2 = new HashMap<>();
        var groups1 = List.of(1, 1, 3);
        var springs1 = "???.###.";
        var groups2 = List.of(1, 1, 3);
        var springs2 = ".??..??...?##..";
        var groups3 = List.of(1, 3, 1, 6);
        var springs3 = "?#?#?#?#?#?#?#?.";
        var groups4 = List.of(4, 1, 1);
        var springs4 = "????.#...#....";
        var groups5 = List.of(1, 6, 5);
        var springs5 = "????.######..#####..";
        var groups6 = List.of(3, 2, 1);
        var springs6 = "?###????????.";

       assertEquals(1, DayTwelve.recursive3(new DayTwelve.State(springs1, groups1, validationMap2)));
        System.out.println();
        System.out.println("Next");
        assertEquals(4, DayTwelve.recursive3(new DayTwelve.State(springs2, groups2, validationMap2)));
        System.out.println();
        System.out.println("Next");
        assertEquals(1, DayTwelve.recursive3(new DayTwelve.State(springs3, groups3, validationMap2)));
        System.out.println();
        System.out.println("Next");
        assertEquals(1, DayTwelve.recursive3(new DayTwelve.State(springs4, groups4, validationMap2)));
        System.out.println();
        System.out.println("Next");
        assertEquals(4, DayTwelve.recursive3(new DayTwelve.State(springs5, groups5, validationMap2)));
        System.out.println();
        System.out.println("Next");
        assertEquals(10, DayTwelve.recursive3(new DayTwelve.State(springs6, groups6, validationMap2)));
    }
}