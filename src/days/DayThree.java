package days;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

public class DayThree {
    public static void run(List<String> convertedInput) {
        int maxY = convertedInput.size() -1 ;
        int maxX = convertedInput.get(0).length() -1 ;
        int totalScore = 0;
        int x = 0;
        int y = 0;

        List<Getalletje> getalletjes = new ArrayList<>();
        for (String s : convertedInput) {

            String currentNumberString = "";
            boolean nearSymbol = false;
            for (char c :
                    s.toCharArray()) {
                if (Character.isDigit(c)) {
                    currentNumberString = currentNumberString + Character.toString(c);
                    if(!nearSymbol) nearSymbol = nearSymbol(convertedInput, x, y, maxX, maxY);
                }
                if (!Character.isDigit(c) && StringUtils.isNotBlank(currentNumberString)) {
                    if (nearSymbol) {
                        totalScore = totalScore + Integer.parseInt(currentNumberString);
                        System.out.println("Total " + totalScore + " with " +  Integer.parseInt(currentNumberString));
                    }
                    getalletjes.add(new Getalletje(Integer.parseInt(currentNumberString), x, y));
                    currentNumberString = "";
                    nearSymbol = false;
                }
                x++;
            }
            if (nearSymbol) {
                getalletjes.add(new Getalletje(Integer.parseInt(currentNumberString), x, y));
                totalScore = totalScore + Integer.parseInt(currentNumberString);
                System.out.println("Total " + totalScore + " with " +  Integer.parseInt(currentNumberString));
            }

            y++;
            x = 0;
        }
        System.out.println("Total score " + totalScore);

        // Part Two
        List<Pair<Integer, Integer>> gearLocations = new ArrayList<>();
        x=0;
        y=0;
        for (String s : convertedInput) {
            for (char c :
                    s.toCharArray()) {
                if(Character.toString(c).equals(gearSymbol)) {
                    gearLocations.add(new ImmutablePair<>(x,y));
                }
                x++;
            }
            y++;
            x = 0;
        }

        int totalGearRatio = 0;
        for (var p :
                gearLocations) {
            List<Getalletje> neighbouringGetalletjes = new ArrayList<>();
            for (var g :
                    getalletjes) {
                if (g.y + 1 == p.getRight() || g.y == p.getRight() || g.y - 1 == p.getRight()) {
                   if(g.range(p.getLeft())){
                       neighbouringGetalletjes.add(g);
                   }
                }
            }
            if(neighbouringGetalletjes.size() == 2) {
                totalGearRatio = totalGearRatio + (neighbouringGetalletjes.get(0).getal * neighbouringGetalletjes.get(1).getal);
            }
        }
        System.out.println("");
        System.out.println("Total Gear Ratio " + totalGearRatio);
    }

    public static boolean nearSymbol(List<String> board, int x, int y, int maxX, int maxY) {
        if(x == 0 && y == 0){
            if (containsSpecialCharacter(Character.toString(board.get(y).charAt(x + 1))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x + 1)))) {
                return true;
            }
        }
        if(x > 0 && y == 0){
            if (containsSpecialCharacter(Character.toString(board.get(y).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y).charAt(x + 1))) ||
                    containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x + 1)))) {
                return true;
            }
        }
        if(x == 0 && y > 0){
            if (containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x))) || containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x + 1))) ||
                    containsSpecialCharacter(Character.toString(board.get(y).charAt(x + 1))) ||
                    containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x + 1)))) {
                return true;
            }
        }
        if(x > 0 && y > 0 && x < maxX && y < maxY){
            if (containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x))) || containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x + 1))) ||
                    containsSpecialCharacter(Character.toString(board.get(y).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y).charAt(x + 1))) ||
                    containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x + 1)))){
                return true;}
        }
        if(x > 0 && y > 0 && x == maxX && y < maxY){
            if (containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x))) ||
                    containsSpecialCharacter(Character.toString(board.get(y).charAt(x - 1))) ||
                    containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y + 1).charAt(x)))){
                return true;}
        }
        if(x > 0 && y > 0 && x < maxX && y == maxY){
            if (containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x))) || containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x + 1))) ||
                    containsSpecialCharacter(Character.toString(board.get(y).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y).charAt(x + 1)))) {
                return true;
            }
        }
        if(x == maxX && y == maxY){
            if (containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x - 1))) || containsSpecialCharacter(Character.toString(board.get(y - 1).charAt(x))) ||
                    containsSpecialCharacter(Character.toString(board.get(y).charAt(x - 1)))) {
                return true;
            }
        }
        return false;
    }
    public static boolean containsSpecialCharacter(String s) {
        return (s == null) ? false : s.matches("[^A-Za-z0-9 .]");
    }

    public static String gearSymbol = "*";

    static class Getalletje{
        int xStart, xEnd, y, getal;

        public Getalletje(int getal, int x, int y){
            this.xStart = x - String.valueOf(getal).length();
            this.xEnd = x - 1;
            this.y = y;
            this.getal = getal;
        }

        public boolean range(int x){
            return Range.of(xStart - 1, xEnd + 1).contains(x);
        }
    }
}
