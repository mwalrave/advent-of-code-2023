package days;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class DayOneTest {

    @ParameterizedTest
    @CsvSource(value = {"12,1", "a12,1"})
    void findFirstNumber(String input, int expected) {
        assertEquals(expected, DayOne.findFirstNumber(input, false));
    }

    @ParameterizedTest
    @CsvSource(value = {"two1nine,2","eightwothree,8", "abcone2threexyz,1", "xtwone3four,2", "4nineeightseven2,4","zoneight234,1", "7pqrstsixteen,7"})
    void find(String input, int expected){
       assertEquals(expected, DayOne.findFirstNumber(input, false));
    }

    @ParameterizedTest
    @CsvSource(value = {"two1nine,9","eightwothree,3", "abcone2threexyz,3", "xtwone3four,4", "4nineeightseven2,2","zoneight234,4", "7pqrstsixteen,6"})
    void findReversed(String input, int expected){
        StringBuilder reversedStr = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            reversedStr.insert(0, input.charAt(i));
        }
       assertEquals(expected, DayOne.findFirstNumber(reversedStr.toString(), true));
    }
}