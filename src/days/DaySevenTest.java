package days;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class DaySevenTest {

    @Test
    void highCard() {
        DaySeven.Hand hand = new DaySeven.Hand("23456", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.HIGH_CARD, hand.handType);
    }

    @Test
    void onePair() {
        DaySeven.Hand hand = new DaySeven.Hand("22456", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.ONE_PAIR, hand.handType);
        hand = new DaySeven.Hand("2J456", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.ONE_PAIR, hand.handType);
    }

    @Test
    void twoPair() {
        DaySeven.Hand hand = new DaySeven.Hand("22446", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.TWO_PAIR, hand.handType);
    }

    @Test
    void threeOfAKind() {
        DaySeven.Hand hand = new DaySeven.Hand("22246", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.THREE_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("22J46", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.THREE_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("2JJ46", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.THREE_OF_A_KIND, hand.handType);
    }

    @Test
    void fourOfAKind() {
        DaySeven.Hand hand = new DaySeven.Hand("22226", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FOUR_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("222J6", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FOUR_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("22JJ4", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FOUR_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("2JJJ4", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FOUR_OF_A_KIND, hand.handType);
    }

    @Test
    void fiveOfAKind() {
        DaySeven.Hand hand = new DaySeven.Hand("22222", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FIVE_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("222J2", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FIVE_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("22JJ2", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FIVE_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("2JJJ2", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FIVE_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("2JJJJ", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FIVE_OF_A_KIND, hand.handType);
        hand = new DaySeven.Hand("JJJJJ", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FIVE_OF_A_KIND, hand.handType);
    }

    @Test
    void fullHouse() {
        DaySeven.Hand hand = new DaySeven.Hand("22233", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FULL_HOUSE, hand.handType);
        hand = new DaySeven.Hand("223J3", 1);
        hand.calculateHand();
        assertEquals(DaySeven.HAND_TYPE.FULL_HOUSE, hand.handType);
    }


}