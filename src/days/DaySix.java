package days;

import java.util.List;

public class DaySix {
    public static void run(List<String> convertedInput) {
        String[] timesSplit= convertedInput.get(0).split(":");
        timesSplit[1] = timesSplit[1].replaceAll(" +", " ");
        timesSplit = timesSplit[1].trim().split(" ");
        String[] distancesSplit = convertedInput.get(1).split(":");
        distancesSplit[1] = distancesSplit[1].replaceAll(" +", " ");
        distancesSplit = distancesSplit[1].trim().split(" ");

        Long[] times = convertStringArrayToIntArray(timesSplit);
        Long[] distances = convertStringArrayToIntArray(distancesSplit);

        long[] speedOptions = new long[times.length];

        for(int i = 0; i < times.length; i++){
            Long minSpeed = calculateMinSpeed(times[i], distances[i]);
            Long maxSpeed = calculateMaxSpeed(times[i], distances[i]);
            speedOptions[i] = maxSpeed - minSpeed + 1;
        }

        long result = speedOptions[0];
        for(int i = 1; i < times.length; i++){
            result *= speedOptions[i];
        }

        System.out.println("Result part one: " + result);

        //Part two
        Long time = Long.parseLong(convertedInput.get(0).split(":")[1].trim().replaceAll(" +", ""));
        Long distance = Long.parseLong(convertedInput.get(1).split(":")[1].trim().replaceAll(" +", ""));
        Long minSpeed = calculateMinSpeed(time, distance);
        Long maxSpeed = calculateMaxSpeed(time, distance);
        System.out.println("Time: " + time + " Distance: " + distance);
        System.out.println("Min: " + minSpeed + " Max: " + maxSpeed);
        System.out.println("Possibilities: " + (maxSpeed - minSpeed + 1));
    }



    public static Long calculateMinSpeed(Long time, Long distance){
        //Min speed
        Long minSpeed = 0L;
        Long speed = 0L;
        while (time > speed){
            if(distance < speed * (time - speed)){
                minSpeed = speed;
                break;
            }
            speed++;
        }
        return minSpeed;
    }

    public static Long calculateMaxSpeed(Long time, Long distance){
        //Min speed
        Long maxSpeed = 0L;
        Long speed = time;
        while (speed > 0){
            if(distance < speed * (time - speed)){
                maxSpeed = speed;
                break;
            }
            speed--;
        }
        return maxSpeed;
    }

    public static Long[] convertStringArrayToIntArray(String[] stringArray){
        Long[] intArray = new Long[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            intArray[i] = Long.parseLong(stringArray[i]);
        }
        return intArray;
    }
}
