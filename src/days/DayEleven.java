package days;


import org.apache.commons.lang3.Range;

import java.util.*;

public class DayEleven {
    public static void run(List<String> convertedInput) {
        int expansionRate = 9;
        int startWidth = convertedInput.get(0).length();
        int startHeight = convertedInput.size();

        List<Node> allNodes = parseNodes(convertedInput);

        List<Integer> xsWithoutPlanets = checkLineForPlanets(startWidth, startHeight, allNodes, true);
        List<Integer> ysWithoutPlanets = checkLineForPlanets(startWidth, startHeight, allNodes, false);

        //printGraph(allNodes, startWidth, startHeight);
        manhattanDistance(allNodes, xsWithoutPlanets, ysWithoutPlanets, expansionRate);
    }

    public static List<Integer> checkLineForPlanets(int startWidth, int startHeight, List<Node> allNodes, boolean xAss) {
        List<Integer> lineWithoutPlanets = new ArrayList<>();
        int counter = xAss ? startWidth : startHeight;
        for (int i = 0; i < counter; i++) {
            final int tempI = i;
            if (allNodes.stream().filter(n -> xAss ? n.x == tempI : n.y == tempI).noneMatch(n -> n.isPlanet))
                lineWithoutPlanets.add(i);
        }
        return lineWithoutPlanets;
    }

    public static List<Node> parseNodes(List<String> input) {
        List<Node> allNodes = new ArrayList<>();
        int currX = 0;
        int currY = 0;
        for (String line :
                input) {
            var split = line.split("");
            for (String s : split) {
                allNodes.add(new Node(currX, currY, s.equals("#")));
                currX++;
            }
            currX = 0;
            currY++;
        }
        return allNodes;
    }

    static void manhattanDistance(List<Node> allNodes, List<Integer> xsWithoutPlanets, List<Integer> ysWithoutPlanets, int expansionRate) {
        var planetNodes = allNodes.stream().filter(n -> n.isPlanet).toList();
        Long totalDistance = 0L;
        List<NodePair> pairs = new ArrayList<>();
        for (Node planet : planetNodes) {
            for (Node destination : planetNodes) {
                if (planet == destination) continue;
                if (pairs.stream().anyMatch(np -> (np.nodeA.equals(planet) && np.nodeB.equals(destination)) || (np.nodeB.equals(planet) && np.nodeA.equals(destination))))
                    continue;
                int tempX = planet.x;
                int tempY = planet.y;
                for (int x : xsWithoutPlanets) {
                    var range = Range.of(destination.x, planet.x);
                    if (range.contains(x)) {
                        if (planet.x > destination.x) {
                            tempX = tempX + expansionRate;
                        } else if (planet.x < destination.x) {
                            tempX = tempX - expansionRate;
                        }
                    }
                }
                for (int y : ysWithoutPlanets) {
                    var range = Range.of(destination.y, planet.y);
                    if (range.contains(y)) {
                        if (planet.y > destination.y) {
                            tempY = tempY + expansionRate;
                        } else if (planet.y < destination.y) {
                            tempY = tempY - expansionRate;
                        }
                    }
                }
                int distance = Math.abs(destination.x - tempX) + Math.abs(destination.y - tempY);
                totalDistance = totalDistance + distance;
                pairs.add(new NodePair(planet, destination, distance));
            }
        }
        System.out.println("Total " + totalDistance);
    }

    static class NodePair {
        Node nodeA;
        Node nodeB;
        int distance;

        public NodePair(Node nodeA, Node nodeB, int distance) {
            this.nodeA = nodeA;
            this.nodeB = nodeB;
            this.distance = distance;
        }
    }

    public static void printGraph(List<Node> graph, int width, int height) {
        Comparator<Node> comparator = Comparator.comparingInt(Node::getY).thenComparing(Node::getX);

        graph.sort(comparator);

        //DRAW GRAPH
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                final int xTemp = x;
                final int yTemp = y;
                var node = graph.stream().filter(n -> n.x == xTemp && n.y == yTemp).findFirst().orElse(null);
                System.out.print(node.isPlanet ? "#" : ".");
            }
            System.out.println();
        }

        System.out.println("Width " + width + " height " + height);
    }


    static class Node {
        public int x;
        public int y;
        boolean isPlanet;

        public Node(int x, int y, boolean isPlanet) {
            this.x = x;
            this.y = y;
            this.isPlanet = isPlanet;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
