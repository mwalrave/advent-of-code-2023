package days;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class DaySeven {
    public static void run(List<String> convertedInput) {
        play(convertedInput);
    }

    public static void play(List<String> convertedInput) {
        Comparator<Hand> handComparator = Comparator.comparing(Hand::getHandTypeValue)
                .thenComparing(Hand::getFirstCard)
                .thenComparing(Hand::getSecondCard)
                .thenComparing(Hand::getThirdCard)
                .thenComparing(Hand::getFourthCard)
                .thenComparing(Hand::getFifthCard);


        List<Hand> hands = new ArrayList<>();
        for (String line :
                convertedInput) {
            var split = line.split(" ");
            hands.add(new Hand(split[0], Integer.parseInt(split[1])));
        }
        for (Hand hand :
                hands) {
            hand.calculateHand();
        }
        hands.sort(handComparator);
        int currRank = 0;
        int totalWinnings = 0;
        for (Hand hand :
                hands) {
            currRank++;
            hand.rank = currRank;
            totalWinnings = totalWinnings + (hand.rank * hand.multiplier);
        }

        System.out.println("Total Winnings: " + totalWinnings);
    }


    enum HAND_TYPE {
        FIVE_OF_A_KIND(6),
        FOUR_OF_A_KIND(5),
        FULL_HOUSE(4),
        THREE_OF_A_KIND(3),
        TWO_PAIR(2),
        ONE_PAIR(1),
        HIGH_CARD(0);

        HAND_TYPE(int value) {
            valueRank = value;
        }

        final int valueRank;

        public int getValueRank() {
            return valueRank;
        }
    }

    public static class Hand {
        String hand;
        int multiplier;
        int rank;
        public HAND_TYPE handType = HAND_TYPE.HIGH_CARD;
        int firstCard;
        int secondCard;
        int thirdCard;
        int fourthCard;
        int fifthCard;
        public Map<String, Integer> valuesMap = Map.ofEntries(
                Map.entry("2", 2),
                Map.entry("3", 3),
                Map.entry("4", 4),
                Map.entry("5", 5),
                Map.entry("6", 6),
                Map.entry("7", 7),
                Map.entry("8", 8),
                Map.entry("9", 9),
                Map.entry("T", 10),
                Map.entry("J", 1),
                Map.entry("Q", 12),
                Map.entry("K", 13),
                Map.entry("A", 14)
        );

        public Hand(String hand, int multiplier) {
            this.hand = hand;
            this.multiplier = multiplier;
        }

        public int getFirstCard() {
            return firstCard;
        }

        public int getSecondCard() {
            return secondCard;
        }

        public int getThirdCard() {
            return thirdCard;
        }

        public int getFourthCard() {
            return fourthCard;
        }

        public int getFifthCard() {
            return fifthCard;
        }

        public int getHandTypeValue() {
            return handType.getValueRank();
        }

        public void calculateHand() {
            firstCard = valuesMap.get(Character.toString(hand.charAt(0)));
            secondCard = valuesMap.get(Character.toString(hand.charAt(1)));
            thirdCard = valuesMap.get(Character.toString(hand.charAt(2)));
            fourthCard = valuesMap.get(Character.toString(hand.charAt(3)));
            fifthCard = valuesMap.get(Character.toString(hand.charAt(4)));

            int amountOfJokers = StringUtils.countMatches(hand, "J");

            Map<String, Integer> valuesAmountOfAppearances = new HashMap<>();

            for (var key : valuesMap.keySet()) {
                int amountOfAppearances = StringUtils.countMatches(hand, key);
                valuesAmountOfAppearances.put(key, amountOfAppearances);
            }

            boolean hasThreeOfAKind = false;
            boolean hasTwoOfAKind = false;
            boolean hasFourOfAKind = false;
            boolean hasFiveOfAKind = false;

            int amountOfPairs = 0;
            for (var entry : valuesAmountOfAppearances.entrySet()) {
                if (entry.getKey().equals("J")) continue;
                if (entry.getValue() == 0) continue;
                if (entry.getValue() == 2) {
                    amountOfPairs++;
                    hasTwoOfAKind = true;
                }
                if (entry.getValue() == 3) {
                    hasThreeOfAKind = true;
                }
                if (entry.getValue() == 4) {
                    hasFourOfAKind = true;
                }
                if (entry.getValue() == 5) {
                    hasFiveOfAKind = true;
                }
            }

            if (hasFiveOfAKind) handType = HAND_TYPE.FIVE_OF_A_KIND;
            else if (hasFourOfAKind && amountOfJokers == 1) handType = HAND_TYPE.FIVE_OF_A_KIND;
            else if (hasThreeOfAKind && amountOfJokers == 2) handType = HAND_TYPE.FIVE_OF_A_KIND;
            else if (hasTwoOfAKind && amountOfJokers == 3) handType = HAND_TYPE.FIVE_OF_A_KIND;
            else if (amountOfJokers == 4) handType = HAND_TYPE.FIVE_OF_A_KIND;
            else if (amountOfJokers == 5) handType = HAND_TYPE.FIVE_OF_A_KIND;
            else if (hasFourOfAKind) handType = HAND_TYPE.FOUR_OF_A_KIND;
            else if (hasThreeOfAKind && amountOfJokers == 1) handType = HAND_TYPE.FOUR_OF_A_KIND;
            else if (hasTwoOfAKind && amountOfJokers == 2) handType = HAND_TYPE.FOUR_OF_A_KIND;
            else if (amountOfJokers == 3) handType = HAND_TYPE.FOUR_OF_A_KIND;
            else if (amountOfPairs == 2 && amountOfJokers == 1) handType = HAND_TYPE.FULL_HOUSE;
            else if (hasThreeOfAKind && hasTwoOfAKind) handType = HAND_TYPE.FULL_HOUSE;
            else if (hasThreeOfAKind) handType = HAND_TYPE.THREE_OF_A_KIND;
            else if (hasTwoOfAKind && amountOfJokers == 1) handType = HAND_TYPE.THREE_OF_A_KIND;
            else if (amountOfJokers == 2) handType = HAND_TYPE.THREE_OF_A_KIND;
            else if (amountOfPairs == 2) handType = HAND_TYPE.TWO_PAIR;
            else if (amountOfPairs == 1) handType = HAND_TYPE.ONE_PAIR;
            else if (amountOfJokers == 1) handType = HAND_TYPE.ONE_PAIR;
            else handType = HAND_TYPE.HIGH_CARD;
        }
    }
}
