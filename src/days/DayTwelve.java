package days;


import java.util.*;

public class DayTwelve {
    public static void run(List<String> convertedInput) {
        doTheThing(convertedInput);
    }

    public static void doTheThing(List<String> input) {
        Long total = 0L;
        for (String line :
                input) {
            Map<State, Long> validationMap = new HashMap<>();
            var split = line.split(" ");
            var groupsStrings = split[1].trim().split(",");
            List<Integer> groups = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                for (String s : groupsStrings) {
                    groups.add(Integer.parseInt(s));
                }
            }
            StringBuilder springsSb = new StringBuilder(split[0]);
            for (int i = 0; i < 4; i++) {
                springsSb.append("?").append(split[0]);
            }
            String springs = springsSb.append(".").toString();
            State firstState = new State(springs, groups, validationMap);
            var springValue = recursive3(firstState);
            total = total + springValue;

        }
        System.out.println("Total is " + total);
    }

    public static Long recursive3(State state) {
        if (state.validationMap.containsKey(state)) {
            return state.validationMap.get(state);
        }
        Long value = 0L;
        if (state.spring.length() == state.springIndex) { //final validation
            if (state.currGroup == 0 && state.groups.size() == state.groupsIndex) {

                value = 1L;

            } else value = 0L;
        } else {
            //hier komen de . # en ? afhandelingen
            if (state.spring.charAt(state.springIndex) == '?') {
                value = value + state.handleHashtag() + state.handleDot();
            } else if (state.spring.charAt(state.springIndex) == '.') {
                value = value + state.handleDot();
            } else if (state.spring.charAt(state.springIndex) == '#') {
                value = value + state.handleHashtag();
            }
        }
        state.validationMap.put(state, value);
        return value;

    }

    static class State {
        String spring;
        List<Integer> groups;
        int groupsIndex;
        int springIndex;
        int currGroup;
        Map<State, Long> validationMap;

        public State(String spring, List<Integer> groups, Map<State, Long> validationMap) {
            this.spring = spring;
            this.groups = groups;
            this.groupsIndex = 0;
            this.springIndex = 0;
            this.currGroup = 0;
            this.validationMap = validationMap;
        }

        public State(String spring, List<Integer> groups, int groupsIndex, int springIndex, int currGroup, Map<State, Long> validationMap) {
            this.spring = spring;
            this.groups = groups;
            this.groupsIndex = groupsIndex;
            this.springIndex = springIndex;
            this.currGroup = currGroup;
            this.validationMap = validationMap;
        }

        public Long handleDot() {
            if (currGroup > 0) {
                if (groups.get(groupsIndex) == currGroup) {
                    return recursive3(new State(spring, groups, groupsIndex + 1, springIndex + 1, 0, validationMap));
                } else {
                    return 0L;
                }
            } else {
                return recursive3(new State(spring, groups, groupsIndex, springIndex + 1, 0, validationMap));
            }
        }

        public Long handleHashtag() {
            if (groupsIndex == groups.size() || currGroup >= groups.get(groupsIndex)) {
                return 0L;
            } else {
                return recursive3(new State(spring, groups, groupsIndex, springIndex + 1, currGroup + 1, validationMap));
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof State state)) return false;

            if (springIndex != state.springIndex) return false;
            if (groupsIndex != state.groupsIndex) return false;
            return currGroup == state.currGroup;
        }

        @Override
        public int hashCode() {
            int result = springIndex;
            result = 31 * result + groupsIndex;
            result = 31 * result + currGroup;
            return result;
        }
    }
}
