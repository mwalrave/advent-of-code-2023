package days;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DayNineTest {

    @Test
    void calcBefore() {
       assertEquals(-1, DayNine.calcBefore(2, 3));
       assertEquals(5, DayNine.calcBefore(2, -3));
       assertEquals(1, DayNine.calcBefore(-2, -3));
       assertEquals(-5, DayNine.calcBefore(-2, 3));
    }
}