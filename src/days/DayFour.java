package days;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class DayFour {
    public static void run(List<String> convertedInput) {
        int score = 0;
        for (String s : convertedInput) {
            int scratchTicketScore = 0;
            var gameSplit = StringUtils.split(s, ":");
            var scratchSplit = StringUtils.split(gameSplit[1], "|");
            var winningNumbers = StringUtils.split(scratchSplit[0], " ");
            var scratchNumbers = StringUtils.split(scratchSplit[1], " ");
            for (String winningNumber : winningNumbers) {
                for (String scratchNumber : scratchNumbers) {
                    if (winningNumber.equals(scratchNumber)) {
                        scratchTicketScore = calcScore(scratchTicketScore);
                    }
                }
            }
            System.out.println("scratchTicketScore " + gameSplit[0] + ": " + scratchTicketScore);
            score += scratchTicketScore;
        }

        System.out.println("Score part one: " + score);

        //Part two
        List<Integer> copies = new ArrayList<>();
        var amountOfTickets = 0;
        for (String s : convertedInput) {
            var gameSplit = StringUtils.split(s, ":");
            var gameId = getNumbersFromString(gameSplit[0]);
            var amountOfCopies = copies.stream().filter(copy -> copy == Integer.parseInt(gameId)).toList().size();
            for (int i = 0; i < amountOfCopies + 1; i++) {
                var scratchSplit = StringUtils.split(gameSplit[1], "|");
                var winningNumbers = StringUtils.split(scratchSplit[0], " ");
                var scratchNumbers = StringUtils.split(scratchSplit[1], " ");
                var copyCount = 0;
                for (String winningNumber : winningNumbers) {
                    for (String scratchNumber : scratchNumbers) {
                        if (winningNumber.equals(scratchNumber)) {
                            copyCount++;
                        }
                    }
                }
                for (int j = 1; j < copyCount + 1; j++) {
                    copies.add(Integer.parseInt(gameId) + j);
                }
                amountOfTickets++;
            }

        }
        System.out.println("Part two: Amount of tickets " + amountOfTickets);
    }

    public static String getNumbersFromString(String str) {
        return str.replaceAll("[^0-9]", "");
    }

    public static int calcScore(int currScore) {
        return currScore == 0 ? currScore + 1 : currScore * 2;
    }
}
