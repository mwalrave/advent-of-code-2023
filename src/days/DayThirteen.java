package days;

import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DayThirteen {

    public static void run(List<String> convertedInput) {
        List<List<Tile>> boards = new ArrayList<>();
        List<Tile> board = new ArrayList<>();
        List<List<String>> boardsStrings = new ArrayList<>();
        List<String> boardLines = new ArrayList<>();
        for (String line :
                convertedInput) {
            if (StringUtils.isBlank(line)) {
                boardsStrings.add(boardLines);
                boardLines = new ArrayList<>();
                continue;
            }
            boardLines.add(line);

        }
        boardsStrings.add(boardLines);

        for (List<String> boardStrings : boardsStrings) {
            int x = 0;
            int y = 0;
            for (String row : boardStrings) {

                for (String col : row.split("")) {
                    board.add(new Tile(x, y, String.valueOf(row.charAt(x))));
                    x++;
                }
                y++;
                x = 0;
            }
            boards.add(board);
            board = new ArrayList<>();
        }

        int total = 0;
        int boardNr = 0;
        for (List<Tile> boardIterator :
                boards) {
            var result = startTheThing2(boardIterator, boardNr);
            total = total + result;
            System.out.println("Board " + boardNr + " returns " + result);
            boardNr++;
        }
        System.out.println();
        System.out.println("Total : " + total);

    }

    public static int startTheThing(List<Tile> board, int boardNumber) {
        int boardHeight = board.stream().filter(t -> t.y == 0).toList().size();
        int boardWidth = board.stream().filter(t -> t.x == 0).toList().size();
        int currentIdenticalCounter = 0;
        for (int i = 0; i < boardHeight; i++) {
            final int tempI = i;
            if (tempI + 1 > boardHeight - 1) continue;

            List<Tile> findLeft = board.stream().filter(t -> t.x == tempI).toList();
            List<Tile> findRight = board.stream().filter(t -> t.x == tempI + 1).toList();
            if (compareLines(findLeft, findRight)) {
                System.out.println("Found vertical mirror on " + i + " for board " + boardNumber);
                var result = foundIdentical(board, false, i, boardWidth, boardHeight);
                if (result > currentIdenticalCounter) currentIdenticalCounter = result;

            }
        }
        for (int i = 0; i < boardWidth; i++) {
            final int tempI = i;
            if (tempI + 1 > boardWidth - 1) continue;

            List<Tile> findLeft = board.stream().filter(t -> t.y == tempI).toList();
            List<Tile> findRight = board.stream().filter(t -> t.y == tempI + 1).toList();
            if (compareLines(findLeft, findRight)) {
                System.out.println("Found horizontal mirror on " + i + " for board " + boardNumber);
                var result = foundIdentical(board, true, i, boardWidth, boardHeight);
                if (result > currentIdenticalCounter) {
                    currentIdenticalCounter = result;
                }

            }
        }
        System.out.println("Identical counter for board " + boardNumber + "  is " + currentIdenticalCounter);
        return currentIdenticalCounter;
    }


    public static int startTheThing2(List<Tile> board, int boardNumber) {
        int boardHeight = board.stream().filter(t -> t.y == 0).toList().size();
        int boardWidth = board.stream().filter(t -> t.x == 0).toList().size();
        boolean horizontal = true;
        Map<Integer, Integer> rowColumnScoreMap = new HashMap<>();
        int currentIdenticalCounter = 0;
        for (int i = 0; i < boardHeight; i++) {
            final int tempI = i;
            if (tempI + 1 > boardHeight - 1) continue;

            List<Tile> findLeft = board.stream().filter(t -> t.x == tempI).toList();
            List<Tile> findRight = board.stream().filter(t -> t.x == tempI + 1).toList();
            if (compareLines(findLeft, findRight)) {
                System.out.println("Found vertical mirror on " + i + " for board " + boardNumber);
                var result = foundIdentical(board, false, i, boardWidth, boardHeight);
                if (result > currentIdenticalCounter) {
                    currentIdenticalCounter = result;
                    rowColumnScoreMap.put(i, result);
                    horizontal = false;
                }

            }
        }
        for (int i = 0; i < boardWidth; i++) {
            final int tempI = i;
            if (tempI + 1 > boardWidth - 1) continue;

            List<Tile> findLeft = board.stream().filter(t -> t.y == tempI).toList();
            List<Tile> findRight = board.stream().filter(t -> t.y == tempI + 1).toList();
            if (compareLines(findLeft, findRight)) {
                System.out.println("Found horizontal mirror on " + i + " for board " + boardNumber);
                var result = foundIdentical(board, true, i, boardWidth, boardHeight);
                if (result > currentIdenticalCounter) {
                    horizontal = true;
                    currentIdenticalCounter = result;
                    rowColumnScoreMap.put(i, result);
                }

            }
        }
        var toReturn = 0;
        var highestValue = 0;
        for (var entry : rowColumnScoreMap.entrySet()) {
            if (entry.getValue() > highestValue) {
                highestValue = entry.getValue();
                toReturn = entry.getKey();
            }
        }
        if (horizontal)
            return (toReturn + 1) * 100;
        return toReturn + 1;
    }

    public static int foundIdentical(List<Tile> board, boolean horizontal, int mirrorIndex, int boardWidth, int boardHeight) {
        int currentIdenticalCounter = 0;
        int leftRightIndex = 0;
        boolean identical = true;
        if (!horizontal) {
            while (boardWidth > mirrorIndex + leftRightIndex && 0 <= mirrorIndex - leftRightIndex && identical) {
                int tempXLeft = mirrorIndex - leftRightIndex;
                int tempXRight = mirrorIndex + leftRightIndex;
                List<Tile> findLeft = board.stream().filter(t -> t.x == tempXLeft).toList();
                List<Tile> findRight = board.stream().filter(t -> t.x == tempXRight + 1).toList();
                if (findRight.isEmpty()) break;
                identical = compareLines(findLeft, findRight);
                if (identical) currentIdenticalCounter++;
                leftRightIndex++;
            }
        } else {
            while (boardHeight > mirrorIndex + leftRightIndex && 0 <= mirrorIndex - leftRightIndex && identical) {
                int tempYLeft = mirrorIndex - leftRightIndex;
                int tempYRight = mirrorIndex + leftRightIndex;
                List<Tile> findLeft = board.stream().filter(t -> t.y == tempYLeft).toList();
                List<Tile> findRight = board.stream().filter(t -> t.y == tempYRight + 1).toList();
                if (findRight.isEmpty()) break;
                identical = compareLines(findLeft, findRight);
                if (identical) currentIdenticalCounter++;
                leftRightIndex++;
            }
        }
        System.out.println();
        System.out.println();
        return currentIdenticalCounter;
    }

    public static boolean compareLines(List<Tile> left, List<Tile> right) {
        boolean identical = true;
        for (int i = 0; i < left.size(); i++) {
            if (!left.get(i).symbol.equals(right.get(i).symbol)) {
                identical = false;
            }
        }
        //     System.out.println("Comparing lines " + identical + " " + left + " and " + right);

        return identical;
    }

    static class Tile {
        int x;
        int y;
        String symbol;

        public Tile(int x, int y, String symbol) {
            this.x = x;
            this.y = y;
            this.symbol = symbol;
        }

        @Override
        public String toString() {
            return symbol;
       /*     return "Tile{" +
                    "x=" + x +
                    ", y=" + y +
                    ", symbol='" + symbol + '\'' +
                    '}';*/
        }
    }
}
