package days;

import java.util.*;

public class DayTen {
    public static void run(List<String> convertedInput) {
        int currX = 0;
        int currY = 0;
        List<Pipe> pipes = new ArrayList<>();
        for (String line :
                convertedInput) {
            for (String symbol :
                    line.split("")) {
                pipes.add(new Pipe(symbol, currX, currY));
                currX++;
            }
            currX = 0;
            currY++;
        }

        Pipe startPipe = pipes.stream().filter(p -> p.symbol.equals("S")).findFirst().get();
        startPipe.symbol = findStartingSymbol(startPipe, pipes);
        startPipe.setDirectionBooleans();
        System.out.println("Startpipe : " + startPipe);
        System.out.println();

        List<Pipe> pathA = new ArrayList<>();
        pathA.add(startPipe);

        Pipe currPipe = startPipe;
        String fromDir = "";
        boolean firstPipe = true;
        while (firstPipe || pathA.get(0) != pathA.get(pathA.size() - 1)) {
            firstPipe = false;
            final Pipe tempPipe = currPipe;
            if (currPipe.hasSouth && !fromDir.equals("north")) {
                Pipe nextPipe = pipes.stream().filter(p -> p.x == tempPipe.x && p.y == tempPipe.y + 1).findFirst().get();
                pathA.add(nextPipe);
                currPipe = nextPipe;
                fromDir = "south";
            } else if (currPipe.hasNorth && !fromDir.equals("south")) {
                Pipe nextPipe = pipes.stream().filter(p -> p.x == tempPipe.x && p.y == tempPipe.y - 1).findFirst().get();
                pathA.add(nextPipe);
                currPipe = nextPipe;
                fromDir = "north";
            } else if (currPipe.hasWest && !fromDir.equals("east")) {
                Pipe nextPipe = pipes.stream().filter(p -> p.x == tempPipe.x - 1 && p.y == tempPipe.y).findFirst().get();
                pathA.add(nextPipe);
                currPipe = nextPipe;
                fromDir = "west";
            } else if (currPipe.hasEast && !fromDir.equals("west")) {
                Pipe nextPipe = pipes.stream().filter(p -> p.x == tempPipe.x + 1 && p.y == tempPipe.y).findFirst().get();
                pathA.add(nextPipe);
                currPipe = nextPipe;
                fromDir = "east";
            } else System.out.println("DEAD END");

            // System.out.println("Next in path dir : " + fromDir + " : " + currPipe);
        }

        System.out.println("Found route?");

        System.out.println("Steps till midpoint : " + countStepsToFurthestPoint(pathA));

        partTwo(pathA);
    }

    public static int countStepsToFurthestPoint(List<Pipe> route) {
        List<Pipe> reversed = new ArrayList<>(route);
        Collections.reverse(reversed);
        int steps = 1;
        Pipe curr = route.get(1);
        Pipe currReversed = reversed.get(1);
        while (curr != currReversed) {
            steps++;
            curr = route.get(steps);
            currReversed = reversed.get(steps);
        }
        return steps;
    }

    public static void partTwo(List<Pipe> route) {
        Long inside = 0L;
        Long outside = 0L;
        route.remove(route.size() - 1);
        boolean insideBool = false;
        String previousSymbol = "";
        for (int i = 0; i < 140; i++) {
            previousSymbol = "";
            insideBool = false;
            for (int j = 0; j < 140; j++) {
                final int x = j;
                final int y = i;
                var result = route.stream().filter(p -> p.x == x && p.y == y).findFirst();
                if (result.isPresent()) {
                    // Spoiler voor wie mijn repo kopieert: Dit zijn de combinaties, waarvan alleen de laatste twee de inside bool om moeten zetten (denk er maar eens over na :) )    F---7, L---J, L---7, F---J
                    if (result.get().symbol.equals("|")) {
                        insideBool = !insideBool;
                    }
                    if (result.get().symbol.equals("7") && previousSymbol.equals("L")) {
                        insideBool = !insideBool;
                    }
                    if (result.get().symbol.equals("J") && previousSymbol.equals("F")) {
                        insideBool = !insideBool;
                    }

                    if(result.get().symbol.equals("|")){
                        previousSymbol = "";
                    }
                    if (!result.get().symbol.equals("-") && !result.get().symbol.equals("|")){
                        //Bij twee tekens die niet - zijn moet de vorige niet meer tellen
                        if(previousSymbol.isEmpty()){
                            previousSymbol = result.get().symbol;
                        }
                       else {
                           previousSymbol = "";
                        }
                    }
                } else {
                    if (insideBool) {
                        inside++;
                        System.out.println("Inside found at x: " + x + " y :" + y);
                    } else outside++;
                }
            }
        }

        System.out.println("Points inside " + inside);
        System.out.println("Points outside " + outside);


    }

    public static String findStartingSymbol(Pipe startPipe, List<Pipe> pipes) {
        boolean westConnects = false;
        boolean northConnects = false;
        boolean southConnects = false;
        boolean eastConnects = false;
        if (startPipe.x > 0 && startPipe.y > 0) {
            if (pipes.stream().filter(p -> p.x == startPipe.x + 1 && p.y == startPipe.y).findFirst().get().hasWest)
                eastConnects = true;
            if (pipes.stream().filter(p -> p.x == startPipe.x - 1 && p.y == startPipe.y).findFirst().get().hasEast)
                westConnects = true;
            if (pipes.stream().filter(p -> p.x == startPipe.x && p.y == startPipe.y + 1).findFirst().get().hasNorth)
                southConnects = true;
            if (pipes.stream().filter(p -> p.x == startPipe.x && p.y == startPipe.y - 1).findFirst().get().hasSouth)
                northConnects = true;
        } else if (startPipe.x > 0) {
            if (pipes.stream().filter(p -> p.x == startPipe.x + 1 && p.y == startPipe.y).findFirst().get().hasWest)
                eastConnects = true;
            if (pipes.stream().filter(p -> p.x == startPipe.x - 1 && p.y == startPipe.y).findFirst().get().hasEast)
                westConnects = true;
            if (pipes.stream().filter(p -> p.x == startPipe.x && p.y == startPipe.y + 1).findFirst().get().hasNorth)
                southConnects = true;
        } else if (startPipe.y > 0) {
            if (pipes.stream().filter(p -> p.x == startPipe.x + 1 && p.y == startPipe.y).findFirst().get().hasWest)
                eastConnects = true;
            if (pipes.stream().filter(p -> p.x == startPipe.x && p.y == startPipe.y + 1).findFirst().get().hasNorth)
                southConnects = true;
            if (pipes.stream().filter(p -> p.x == startPipe.x && p.y == startPipe.y - 1).findFirst().get().hasSouth)
                northConnects = true;
        }

        if (northConnects && southConnects) {
            return "|";
        }
        if (westConnects && eastConnects) {
            return "-";
        }
        if (northConnects && eastConnects) {
            return "L";
        }
        if (northConnects && westConnects) {
            return "J";
        }
        if (southConnects && westConnects) {
            return "7";
        }
        if (southConnects && eastConnects) {
            return "F";
        }
        return ".";
    }

    static class Pipe {
        String symbol;
        public int x;
        public int y;

        boolean hasNorth;
        boolean hasSouth;
        boolean hasWest;
        boolean hasEast;
        boolean startTile;


        public Pipe(String symbol, int x, int y) {
            this.symbol = symbol;
            this.x = x;
            this.y = y;
            this.startTile = false;
            setDirectionBooleans();
        }

        public void setDirectionBooleans() {
            switch (symbol) {
                case "|":
                    setBooleans(true, false, true, false);
                    break;
                case "-":
                    setBooleans(false, true, false, true);
                    break;
                case "L":
                    setBooleans(true, true, false, false);
                    break;
                case "J":
                    setBooleans(true, false, false, true);
                    break;
                case "7":
                    setBooleans(false, false, true, true);
                    break;
                case "F":
                    setBooleans(false, true, true, false);
                    break;
                case ".":
                    setBooleans(false, false, false, false);
                    break;
                case "S":
                    this.startTile = true;
                    break;
            }
        }

        public void setBooleans(boolean north, boolean east, boolean south, boolean west) {
            hasNorth = north;
            hasEast = east;
            hasSouth = south;
            hasWest = west;
        }


        @Override
        public String toString() {
            return "Pipe{" +
                    "symbol='" + symbol + '\'' +
                    ", x=" + x +
                    ", y=" + y +
                    ", hasNorth=" + hasNorth +
                    ", hasSouth=" + hasSouth +
                    ", hasWest=" + hasWest +
                    ", hasEast=" + hasEast +
                    ", startTile=" + startTile +
                    '}';
        }
    }
}
