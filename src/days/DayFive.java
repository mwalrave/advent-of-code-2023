package days;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DayFive {
    public static void run(List<String> convertedInput) {
        List<Long> seeds = new ArrayList<>();
        List<List<String>> sections = new ArrayList<>();
        List<List<List<Long>>> sectionsLong = new ArrayList<>();

        List<String> tempSection = new ArrayList<>();
        for (String line :
                convertedInput) {
            if (StringUtils.contains(line, "seeds:")) {
                String[] split = line.split(":")[1].trim().split(" ");
                Arrays.stream(split).forEach(s -> {
                    seeds.add(Long.parseLong(s));
                });
                continue;
            }

            if (StringUtils.equals(line.trim(), "")) {
                if (!tempSection.isEmpty()) sections.add(tempSection);
                tempSection = new ArrayList<>();
            }
            if (StringUtils.getDigits(line).isEmpty()) continue;

            else {
                tempSection.add(line);
            }
        }
        sections.add(tempSection);

        List<List<Long>> temp = new ArrayList<>();
        for (var section :
                sections) {
            List<Long> temp2 = new ArrayList<>();
            for (var subSection :
                    section) {
                var split = subSection.split(" ");
                for (var spl : split) {
                    temp2.add(Long.parseLong(spl));
                }
                temp.add(temp2);
                temp2 = new ArrayList<>();
            }
            sectionsLong.add(temp);
            temp = new ArrayList<>();
        }

        var lowestFinalDestination = Long.MAX_VALUE;
        for (Long seed :
                seeds) {
            var seedEndpoint = calculateDestination(seed, sectionsLong);
            if(seedEndpoint < lowestFinalDestination) lowestFinalDestination = seedEndpoint;
        }
    
        System.out.println("Lowest Final Destination: " + lowestFinalDestination);

        //part two
        var lowestFinalDestinationWithSeedRanges = Long.MAX_VALUE;

        for(int i = 0; i < seeds.size(); i = i+2){
            for(Long j = seeds.get(i); j < seeds.get(i) + seeds.get(i+1); j++){
                    var seedEndpoint = calculateDestination(j, sectionsLong);
                    if(seedEndpoint < lowestFinalDestinationWithSeedRanges) {
                        lowestFinalDestinationWithSeedRanges = seedEndpoint;
                    }
                }
            }

        System.out.println("Lowest Final Destination With Seed Ranges (part two): " + lowestFinalDestinationWithSeedRanges);
    }

    public static Long calculateDestination(Long seed, List<List<List<Long>>> map) {
        Long newDestination = seed;
        for (List<List<Long>> list : map) {
            for (List<Long> subList :
                    list) {
                var range = Range.of(subList.get(1), subList.get(1) + subList.get(2));
                if (range.contains(newDestination)) {
                    newDestination = subList.get(0) + (newDestination - subList.get(1));
                    break;
                }
            }
        }
        return newDestination;
    }
}
