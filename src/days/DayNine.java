package days;

import java.util.ArrayList;
import java.util.List;

public class DayNine {

    public static void run(List<String> convertedInput) {
        Long sum = 0L;
        for (String line :
                convertedInput) {

            var historyReport = buildHistoryReport(line);
            sum = sum + partOne(historyReport);
        }
        System.out.println("Sum part one: " + sum);

        sum = 0L;
        for (String line :
                convertedInput) {
            var historyReport = buildHistoryReport(line);
            sum = sum + partTwo(historyReport);
        }
        System.out.println();
        System.out.println("Sum part two: " + sum);
    }

    public static Long partOne(List<List<Integer>> list){
        list.get(list.size()-1).add(0);
        for (int i = list.size()-1 - 1; i > -1; i--) {
            int nextValue = list.get(i).get(list.get(i).size() - 1) + list.get(i + 1).get(list.get(i + 1).size() - 1);
            list.get(i).add(nextValue);
            if (i == 0) return Long.parseLong(String.valueOf(nextValue));
        }
        return 0L;
    }

    public static Long partTwo(List<List<Integer>> list){
        list.get(list.size()-1).add(0, 0);
        for (int i = list.size()-1 - 1; i > -1; i--) {

            int nextValue = calcBefore(list.get(i).get(0), list.get(i + 1).get(0));
            list.get(i).add(0, nextValue);
            if (i == 0)
                return Long.parseLong(String.valueOf(nextValue));

        }
        return 0L;
    }

    public static List<Integer> convertInputToIntList(String line) {
        var split = line.split(" ");
        List<Integer> converted = new ArrayList<>();
        for (String s :
                split) {
            converted.add(Integer.parseInt(s));
        }
        return converted;
    }


    public static List<List<Integer>> buildHistoryReport(String line){
        List<List<Integer>> list = new ArrayList<>();
        list.add(convertInputToIntList(line));
        List<Integer> diffies = new ArrayList<>();
        int currList = 0;
        while (!onlyZeroes(list.get(currList))) {
            for (int i = 0; i < list.get(currList).size(); i++) {
                if (i == list.get(currList).size() - 1) break;
                var first = list.get(currList).get(i);
                var second = list.get(currList).get(i + 1);
                diffies.add(second - first);
            }
            list.add(diffies);
            diffies = new ArrayList<>();
            currList++;
        }
        return list;
    }

    public static int calcBefore(int first, int second) {
        if (first >= 0 && second >= 0) return first - second;
        else if (first >= 0) return first + (second * -1);
        else if (second < 0) return first - second;
        else return first - second;
    }

    public static boolean onlyZeroes(List<Integer> list) {
        for (Integer i :
                list) {
            if (i != 0) return false;
        }
        return true;
    }
}
