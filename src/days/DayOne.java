package days;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class DayOne {
    public static void run(List<String> convertedInput) {
        int sum = 0;
        for (String line :
                convertedInput) {
            StringBuilder reversedStr = new StringBuilder();
            for (int i = 0; i < line.length(); i++) {
                reversedStr.insert(0, line.charAt(i));
            }
            sum = sum + Integer.parseInt(findFirstNumber(line, false) + String.valueOf(findFirstNumber(reversedStr.toString(), true)));
        }
        System.out.println("Total sum = " + sum);
    }

    public static int findFirstNumber(String line, boolean reversed) {
        StringBuilder currString = new StringBuilder();
        for (char c :
                line.toCharArray()) {
            if (Character.isDigit(c)) {
                if (StringUtils.isNotBlank(currString.toString())) {
                    int writtenNumber = checkIfWrittenNumber(currString.toString(), reversed);
                    if (writtenNumber > 0 && writtenNumber < 10) return writtenNumber;
                }
                return Character.getNumericValue(c);
            } else currString.append(c);
        }
        if (StringUtils.isNotBlank(currString.toString())) {
            int writtenNumber = checkIfWrittenNumber(currString.toString(), reversed);
            if (writtenNumber > 0 && writtenNumber < 10) return writtenNumber;
        }
        return 0;
    }

    public static int checkIfWrittenNumber(String s, boolean reversed) {
        List<String> numbersToCheck = reversed ? writtenNumbersReversed : writtenNumbers;

        List<String> foundNumbers = new ArrayList<>();
        for (String number : numbersToCheck) {
            if (StringUtils.contains(s, number)) foundNumbers.add(number);
        }

        Pair<String, Integer> firstNumber = new MutablePair<>("", 999);
        for (String number :
                foundNumbers) {
            int index = StringUtils.indexOf(s, number);
            if (firstNumber.getRight() > index) firstNumber = new MutablePair<>(number, index);
        }
        return numbersToCheck.indexOf(firstNumber.getLeft()) + 1;
    }

    public static List<String> writtenNumbers = List.of("one", "two", "three", "four", "five", "six", "seven", "eight", "nine");
    public static List<String> writtenNumbersReversed = List.of("eno", "owt", "eerht", "ruof", "evif", "xis", "neves", "thgie", "enin");
}
