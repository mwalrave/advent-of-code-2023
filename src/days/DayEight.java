package days;

import utils.LeastCommonMultiplier;
import java.util.*;

public class DayEight {

    public static void run(List<String> convertedInput) {
        List<String> instructions = Arrays.stream(convertedInput.get(0).split("")).toList();

        List<Place> places = new ArrayList<>();
        for (String line :
                convertedInput) {
            if (line.contains(" = ")) {
                var split = line.split(" = ");
                var leftRightSplit = split[1].replace("(", "").replace(")", "").split(", ");
                places.add(new Place(split[0], leftRightSplit[0], leftRightSplit[1]));
            }
        }
 
        //Part two
        partTwo(places, instructions);

    }

    static void partTwo(List<Place> places, List<String> instructions) {
        List<Place> currPlaces = places.stream().filter(place -> place.endsWith.equals("A")).toList();

        List<Long> stepsList = new ArrayList<>();
        Long currentLCM = 1L;
        Long steps = 0L;
        for (Place p :
                currPlaces) {
            steps = 0L;
            boolean foundZ = false;
            while(!foundZ){

                for (String instruction :
                        instructions) {
                    if (instruction.equals("L")) {
                        final String tempPlaceName = p.left;
                        p = places.stream().filter(place -> place.placeName.equals(tempPlaceName)).findFirst().get();
                    }
                    if (instruction.equals("R")) {
                        final String tempPlaceName = p.right;
                        p = places.stream().filter(place -> place.placeName.equals(tempPlaceName)).findFirst().get();

                        if(tempPlaceName.endsWith("Z")){
                            stepsList.add(steps);
                            foundZ = true;
                        }
                    }
                    steps++;
                }
            }
            currentLCM = LeastCommonMultiplier.lcm(currentLCM, steps);
        }

        System.out.println("LCM "  + currentLCM);
    }

        static void partOne(List<String> instructions, List<Place> places) {
        int steps = 0;
        Place currPlace = places.stream().filter(place -> place.placeName.equals("AAA")).findFirst().get();
        boolean foundZZZ = false;
        while (!foundZZZ) {
            for (String instruction :
                    instructions) {
                if (instruction.equals("L")) {
                    final String tempPlaceName = currPlace.left;
                    currPlace = places.stream().filter(place -> place.placeName.equals(tempPlaceName)).findFirst().get();
                }
                if (instruction.equals("R")) {
                    final String tempPlaceName = currPlace.right;
                    currPlace = places.stream().filter(place -> place.placeName.equals(tempPlaceName)).findFirst().get();
                }
                steps++;
                if (currPlace.placeName.equals("ZZZ")) {
                    System.out.println("Found ZZZ");
                    foundZZZ = true;
                    break;
                }
            }
        }
        System.out.println("Part one: steps: " + steps);
    }

    static class Place {
        String placeName;
        String left;
        String right;
        String endsWith;


        public Place(String placeName, String left, String right) {
            this.placeName = placeName;
            this.left = left;
            this.right = right;
            this.endsWith = placeName.substring(placeName.length() - 1);
        }
    }
}
