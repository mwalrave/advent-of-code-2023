package days;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class DayTwo {
    public static void run(List<String> convertedInput) {
        int gameIdTotal = 0;
        int cumulativePower = 0;
        for (String line :
                convertedInput) {
            gameIdTotal = gameIdTotal + checkGamePossible(line);
            cumulativePower = cumulativePower + checkMinimumPower(line);
        }
        System.out.println("Games possible: " + gameIdTotal);
        System.out.println("Cumulative power: " + cumulativePower);
    }

    public static int checkGamePossible(String line) {
        var split = StringUtils.split(line, ":");
        String game = split[0];
        String rest = split[1];
        var setsSplit = StringUtils.split(rest, ";");
        for (String set :
                setsSplit) {
            int totalBlue = 0;
            int totalRed = 0;
            int totalGreen = 0;
            var setSplit = StringUtils.split(set, ",");
            for (String cube :
                    setSplit) {
                int amount = Integer.parseInt(cube.replaceAll("[^0-9]", ""));

                if (StringUtils.contains(cube, "green")) {
                    totalGreen = totalGreen + amount;
                }
                if (StringUtils.contains(cube, "blue")) {
                    totalBlue = totalBlue + amount;
                }
                if (StringUtils.contains(cube, "red")) {
                    totalRed = totalRed + amount;
                }
            }
            if (totalGreen > 13 || totalRed > 12 || totalBlue > 14) return 0;
        }

        return Integer.parseInt(StringUtils.getDigits(game));
    }

    public static int checkMinimumPower(String line){
        var minBlue = 0;
        var minRed = 0;
        var minGreen = 0;

        var split = StringUtils.split(line, ":");
        String rest = split[1];
        var setsSplit = StringUtils.split(rest, ";");
        for (String set :
                setsSplit) {
            var setSplit = StringUtils.split(set, ",");
            for (String cube :
                    setSplit) {
                int amount = Integer.parseInt(cube.replaceAll("[^0-9]", ""));

                if (StringUtils.contains(cube, "green")) {
                    if(amount > minGreen) minGreen = amount;
                }
                if (StringUtils.contains(cube, "blue")) {
                    if(amount > minBlue) minBlue = amount;
                }
                if (StringUtils.contains(cube, "red")) {
                    if(amount > minRed) minRed = amount;
                }
            }
        }

        return calculatePower(minBlue, minGreen, minRed);
    }

    public static int calculatePower(int blue, int green, int red) {
        return blue * green * red;
    }
}
